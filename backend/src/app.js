let express = require('express');
let cors = require('cors');
let app = express();


//Settings 
app.set('port', 4000);

//Middlewares: Funciones a ejecutar antes de llegar a las URL
 //cors Permite coder conectar servidores entre ellos
 app.use(cors());
 app.use(express.json());

//Routes

app.use('/', require('./routes/pinicio'));
app.use('/nivelaciongeodesica', require('./routes/nivelaciongeodesica'));
app.use('/nivelaciontrigonometrica', require('./routes/nivelaciontrigonometrica'));
app.use('/biseccion', require('./routes/biseccion'));
app.use('/triseccion', require('./routes/triseccion'));
app.use('/directoeinverso', require('./routes/directoeinverso'));

module.exports = app;