import React from 'react';
//import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';

import Principal from './components/pprincipal';
import Biseccion from './components/pbiseccion';
import Triseccion from './components/ptriseccion';
import NivelacionG from './components/pnivelaciongeodesica';
import NivelacionT from './components/pnivelaciontrigonometrica';
import DirInver from './components/pdirectoeinverso';

function App(){

    return (
        <Router>
            <Route exact path='/' component={Principal}/>
            <Route exact path='/Biseccion' component={Biseccion}/>
            <Route exact path='/Triseccion' component={Triseccion}/>
            <Route exact path='/NivelacionGeodesica' component={NivelacionG}/>
            <Route exact path='/NivelacionTrigonometrica' component={NivelacionT}/>
            <Route exact path='/DirectoInverso' component={DirInver}/>
        </Router>
    );
}

export default App; 