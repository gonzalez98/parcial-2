import React, { Component } from 'react';
import { MapContainer, TileLayer, Polygon } from 'react-leaflet';
import './style/pbiseccion.css';
import {Lat1, Lat2, Lat3, Lon1, Lon2, Lon3} from './pbiseccion';
class Mapa extends Component {


    render() {
        var LatA = Lat1;
        var LonA = Lon1;
        var LatB = Lat2;
        var LonB = Lon2;
        var LatP = Lat3;
        var LonP = Lon3;
        var polygon = [
            [LatA, LonA],
            [LatB, LonB],
            [LatP, LonP],
            ]
        var options = { color: 'black' }
        console.log(LatP);
        return (
            <MapContainer className="map" center={[5, 74]} zoom={5} scrollWheelZoom={true}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />

            </MapContainer>
        );
    }
}
export default Mapa;

//<Polygon pathOptions={options} positions={polygon} />