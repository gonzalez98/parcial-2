//Nivelacion Trigonometrica Geodesica
import React from 'react';
import { Container, Button, Nav, Navbar} from 'react-bootstrap';
import './style/pnivelaciont.css'
class NivelacionT extends React.Component {

    render() {

        let elemento =

        <Container>
        <Navbar bg="dark" variant="dark" className='nav'>
            <Navbar.Brand href="/">GEODESIA GEOMETRICA</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href=""></Nav.Link>
                <Nav.Link href="/Triseccion">Nivelación Geodésica</Nav.Link>
            </Nav>
        </Navbar>
        <br></br>
        <h1 className="titulo">Nivelación Trigonométrica Geodésica</h1>
        <div className="general">
            <div className="general1"> 
                <div>
                    
                </div>
                <div className="texto">
                    <h5>-
                    </h5>
                </div>
            </div>
            <div className="general2">
                <div className="general21">
                    <label></label>
                    <select className='select'>
                        <option value='reciproco'>Método recíproco</option>
                        <option value='noreciproco'>Método no recíproco</option>
                    </select><br></br>
                    <table className="f">
                        <tr>
                            <td>
                                <label> Ángulo Φ: </label>
                                <br></br>
                                <label> Ángulo ρ: </label>
                                <br></br>
                                <label> Altura intrumental: </label>
                                <br></br>
                                <label> Altura de la torre: </label>
                                <br></br>
                                <label> Cota conocida: </label>
                                <br></br>
                                <label> Distancia P1-P2: </label>
                                <br></br>
                                <label> Ángulo Z1: </label>
                                <br></br>
                                <label> Ángulo Z2: </label>
                                <br></br>
                            </td>
                            <td>
                                <input type="number" id="phi"></input>
                                <br></br>
                                <input type="number" id="ro"></input>
                                <br></br>
                                <input type="number" id="Ai"></input>
                                <br></br>
                                <input type="number" id="At"></input>
                                <br></br>
                                <input type="number" id="Cota"></input>
                                <br></br>
                                <input type="number" id="P12"></input>
                                <br></br>
                                <input type="number" id="Z1"></input>
                                <br></br>
                                <input type="number" id="Z2"></input>
                            </td>
                        </tr><br></br><br></br>
                    </table>
                    <Button className="button" variant='outline-dark' onClick={this.Metodos}>Calcular</Button>
                    <br></br><br></br>
                    <label> Excentricidad:</label><span id="e"></span>
                    <br></br>
                    <label> Sección Meridiana:</label><span id="M"></span>
                    <br></br>
                    <label> Primer vertical:</label><span id="N"></span>
                    <br></br>
                    <label> Azimut Cualquiera:</label><span id="Ac"></span>
                    <br></br>
                    <label> Sección Promedio:</label><span id="Sprom"></span>
                    <br></br>
                    <label> A:</label><span id="A"></span>
                    <br></br>
                    <label> B:</label><span id="B"></span>
                    <br></br>
                    <label> C:</label><span id="C"></span>
                    <br></br>
                    <label> Correcion de H2:</label><span id="H2c"></span>
                    <br></br>
                    <label> Cota desconocia:</label><span id="H2"></span>
                    <br></br>
                    <span id='K'></span>
                    <br></br>
                    <Button className="button" variant='outline-dark' onClick={this.cMapa}>Graficar</Button>
                </div>
                <div className="visor">
                    
                </div>
            </div>     
        </div>               
    </Container >;
            
        return elemento;
    }

    Metodos(){
        
        var a = 6378137.000;      //Semieje mayor del elipsoide de referencia (WGS84)
        var b = 6356752.3141;
        var phi = parseFloat(document.getElementById('phi').value);
        var ro = parseFloat(document.getElementById('ro').value);
        var Ai = parseFloat(document.getElementById('Ai').value);
        var At = parseFloat(document.getElementById('At').value);
        var Cota = parseFloat(document.getElementById('Cota').value);
        var P12 = parseFloat(document.getElementById('P12').value);
        var Z1 = parseFloat(document.getElementById('Z1').value);
        var Z2 = parseFloat(document.getElementById('Z2').value);

        var e = ((a*a)-(b*b))/(a*a); //
        var K = Math.pow(Math.sin(phi*(Math.PI/180)),2);
        var c = 1-(e*K);
        var P = Math.pow(c, 3);
        var M = (a*(1-e))/(Math.sqrt(P)); //Meridiana
        var N = (a/(Math.sqrt(c))); //Primer Vertical 
        var J = (Math.sin(ro*(Math.PI/180)))*(Math.sin(ro*(Math.PI/180)));
        var Y = (Math.cos(ro*(Math.PI/180)))*(Math.cos(ro*(Math.PI/180)));
        var u = ((M*J)+(N*Y));
        var Ac = ((M*N)/u);  //Azimut cualquiera
        var Sprom = Math.sqrt(M*N);

        let miElemento = document.querySelector('select');
        if (miElemento.value === 'reciproco') {


            document.getElementById('e').innerHTML=" "+e;
            document.getElementById('M').innerHTML=" "+M;
            document.getElementById('N').innerHTML=" "+N;
            document.getElementById('Ac').innerHTML=" "+Ac;
            document.getElementById('Sprom').innerHTML=" "+Sprom;

            let d = 0.0002777777778;
            let t = P12*Math.sin(d*(Math.PI/180));
            let Corr = (Ai-At)/t;
            let A = 1+(Cota/Ac);
            let w = Math.tan(((Z2-Z1)/2)*(Math.PI/180));
            let B = 1+((P12*w)/(2*Ac));
            let g = (P12)*(P12);
            let r = 12*Ac;
            let C = 1+(g/r);
            let s = ((P12)*(w)*(A)*(B)*(C));
            let H2 = ((s)+(Cota));
            let H2c = ((H2)+(Corr));

            document.getElementById('A').innerHTML=" "+A;
            document.getElementById('B').innerHTML=" "+B;
            document.getElementById('C').innerHTML=" "+C;
            document.getElementById('H2').innerHTML=" "+H2;
            document.getElementById('H2c').innerHTML=" "+H2c;
            
        } else {

            document.getElementById('e').innerHTML=" "+e;
            document.getElementById('M').innerHTML=" "+M;
            document.getElementById('N').innerHTML=" "+N;
            document.getElementById('Ac').innerHTML=" "+Ac;
            document.getElementById('Sprom').innerHTML=" "+Sprom;

            let d = 0.0002777777778;
            let t = Math.sin(d*(Math.PI/180));
            let w = P12*(0.5-0.071);
            let g = (Ac*t);
            let Kk = (w/g);
            let A = 1+(Cota/Ac);
            let B = 1+((P12*w)/(2*Ac));
            let I = Math.tan((90+K-Z1)*(Math.PI/180));
            let f = (P12)^2;
            let Z = 12*((Ac)^2);
            let C = 1+(f/Z);
            let h2 = (P12*I*A*B*C)+Cota;           
            let Corr = (Ai-At);
            
            document.getElementById('A').innerHTML=" "+A;
            document.getElementById('B').innerHTML=" "+B;
            document.getElementById('C').innerHTML=" "+C;
            document.getElementById('H2').innerHTML=" "+h2;
            document.getElementById('H2c').innerHTML=" "+Corr;
            document.getElementById('K').innerHTML="K: "+Kk;
        }        
    }
}

export default NivelacionT;