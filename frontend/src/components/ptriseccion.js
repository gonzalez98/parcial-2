//Triseccion
import React from 'react';
import { Container, Button, Nav, Navbar} from 'react-bootstrap';
import './style/ptriseccion.css';
import Mapa from './mapa';
//import Imagen from './style/images/biseccion.JPG';
class Triseccion extends React.Component {

    render() {

        let elemento =

        <Container>
        <Navbar bg="dark" variant="dark" className='nav'>
            <Navbar.Brand href="/">GEODESIA GEOMETRICA</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href=""></Nav.Link>
                <Nav.Link href="/Biseccion">Bisección</Nav.Link>
            </Nav>
        </Navbar>
        <br></br>
        <h1 className="titulo">Trisección</h1>
        <div className="general">
            <div className="general1"> 
                <div>
                    
                </div>
                <div className="texto">
                    <h5>El metodo de trisección consiste en determinar las coordenadas de un punto P (NP, EP) conocidos tres vértices: A (NA, EA) y
                        B (NB, EB), C (NC, EC) y los angulos comprendidos.
                    </h5>
                </div>
            </div>

            <div className="general2">
                <div className="general21">
                    <label>Coordenadas planas cartesianas locales (Origen Bogotá)</label>
                    <select className='select'>
                        <option value='caso1'>Caso 1</option>
                        <option value='caso2'>Caso 2</option>
                        <option value='caso3'>Caso 3</option>
                    </select><br></br>
                    <table className="f">
                        <tr>
                            <td>
                                <label> Norte de A: </label>
                                <br></br>
                                <label> Este de A: </label>
                                <br></br>
                                <label> Norte de B: </label>
                                <br></br>
                                <label> Este de B: </label>
                                <br></br>
                                <label> Norte de C: </label>
                                <br></br>
                                <label> Este de C: </label>
                                <br></br>
                                <label> Angulo X: </label>
                                <br></br>
                                <label> Angulo Y: </label>
                                <br></br>
                                <label> Angulo Z: </label>
                            </td>
                            <td>
                                <input type="number" id="NA"></input>
                                <br></br>
                                <input type="number" id="EA"></input>
                                <br></br>
                                <input type="number" id="NB"></input>
                                <br></br>
                                <input type="number" id="EB"></input>
                                <br></br>
                                <input type="number" id="NC"></input>
                                <br></br>
                                <input type="number" id="EC"></input>
                                <br></br>
                                <input type="number" id="X"></input>
                                <br></br>
                                <input type="number" id="Y"></input>
                                <br></br>
                                <input type="number" id="Z"></input>
                            </td>
                        </tr><br></br><br></br>
                    </table>
                    <Button className="button" variant='outline-dark' onClick={this.ctriseccion}>Calcular</Button>
                    <br></br><br></br>
                    <label> Norte de P</label>
                    <span id="NP"></span>
                    <br></br>
                    <label> Este de P</label>
                    <span id="EP"></span>
                    <br></br>
                    <Button className="button" variant='outline-dark' onClick={this.cMapa}>Graficar</Button>
                </div>
                <div className="visor">
                    <Mapa/>
                </div>
            </div>     
        </div>               
    </Container >;

        return elemento;
    }

    ctriseccion(){

        var NA = parseFloat(document.getElementById("NA").value);
        var NB = parseFloat(document.getElementById("NB").value);
        var EA = parseFloat(document.getElementById("EA").value);
        var EB = parseFloat(document.getElementById("EB").value);
        var NC = parseFloat(document.getElementById("NC").value);
        var EC = parseFloat(document.getElementById("EC").value);
        var X = parseFloat(document.getElementById("X").value) ;
        var Y = parseFloat(document.getElementById("Y").value) ;
        var Z = parseFloat(document.getElementById("Z").value) ;
        var Sa = parseFloat(X+Y+Z);
        var L = 360;
        var L2 = 720;
        
        let miElemento = document.querySelector('select');
        if (miElemento.value==='caso1') {
            if (Sa === L) {
                
                let A = ((Math.atan(((EC-EA)/(NC-NA)))*(180/Math.PI))-(Math.atan(((EB-EA)/(NB-NA)))*(180/Math.PI)));
                let B = ((Math.atan(((EA-EB)/(NA-NB)))*(180/Math.PI))-(Math.atan(((EC-EB)/(NC-NB)))*(180/Math.PI)));
                let C = ((Math.atan(((EB-EC)/(NB-NC)))*(180/Math.PI))-(Math.atan(((EA-EC)/(NA-NC)))*(180/Math.PI)));

                let K1 = ((1)/(((Math.cos(A*(Math.PI/180)))/(Math.sin(A*(Math.PI/180))))-((Math.cos(X*(Math.PI/180)))/(Math.sin(X*(Math.PI/180))))));
                let K2 = ((1)/(((Math.cos(B*(Math.PI/180)))/(Math.sin(B*(Math.PI/180))))-((Math.cos(Y*(Math.PI/180)))/(Math.sin(Y*(Math.PI/180))))));
                let K3 = ((1)/(((Math.cos(C*(Math.PI/180)))/(Math.sin(C*(Math.PI/180))))-((Math.cos(Z*(Math.PI/180)))/(Math.sin(Z*(Math.PI/180))))));

                let EP = ((K1*EA)+(K2*EB)+(K3*EC))/(K1+K2+K3);
                let NP = ((K1*NA)+(K2*NB)+(K3*NC))/(K1+K2+K3);

                document.getElementById('EP').innerHTML=": "+EP;
                document.getElementById('NP').innerHTML=": "+NP;

                console.log(A);
                console.log(B);
                console.log(C);
                console.log(K1);
                console.log(K2);
                console.log(K3);

            
            
            
            
            } else {alert('La suma de los angulos no cumplen la condición: (X+Y+Z) = 360') }
        } else 
            if (miElemento.value ==='caso2') {
                if (Sa === L) {

                    let t = ((EC-EA)/(NC-NA));
                    let h = ((EB-EA)/(NB-NA));
                    let s = ((EA-EB)/(NA-NB));
                    let g = ((EC-EB)/(NC-NB));
                    let n = ((EB-EC)/(NB-NC));
                    let v = ((EA-EC)/(NA-NC));

                    let A = ((Math.atan(t)*(180/Math.PI))-(Math.atan(h)*(180/Math.PI)));
                    let B = ((Math.atan(s)*(180/Math.PI))-(Math.atan(g)*(180/Math.PI)));
                    let C = ((Math.atan(n)*(180/Math.PI))-(Math.atan(v)*(180/Math.PI)));

                    let f1 = ((Math.cos(A*(Math.PI/180)))/(Math.sin(A*(Math.PI/180))));
                    let q1 = ((Math.cos(X*(Math.PI/180)))/(Math.sin(X*(Math.PI/180))));
                    let K1 = (1/(f1-q1));
                    
                    let f2 = ((Math.cos(B*(Math.PI/180)))/(Math.sin(B*(Math.PI/180))));
                    let q2 = ((Math.cos(Y*(Math.PI/180)))/(Math.sin(Y*(Math.PI/180))));
                    let K2 = (1/(f2-q2));

                    let f3 = ((Math.cos(C*(Math.PI/180)))/(Math.sin(C*(Math.PI/180))));
                    let q3 = ((Math.cos(Z*(Math.PI/180)))/(Math.sin(Z*(Math.PI/180))));
                    let K3 = (1/(f3-q3));

                    let EP = (((K1*EA)+(K2*EB)+(K3*EC))/(K1+K2+K3));
                    let NP = (((K1*NA)+(K2*NB)+(K3*NC))/(K1+K2+K3));           

                    document.getElementById('EP').innerHTML=": "+EP;
                    document.getElementById('NP').innerHTML=": "+NP;

                } else {alert('La suma de los angulos no cumplen la condición: (X+Y+Z) = 360') }       
            } else 
                if (miElemento.value === 'caso3'){
                    if (Sa === L2 && (Z<X) && (Z<Y) && (X<Y)) {
                
                        let A = ((Math.atan(((EC-EA)/(NC-NA)))*(180/Math.PI))-(Math.atan(((EB-EA)/(NB-NA)))*(180/Math.PI)));
                        let B = ((Math.atan(((EA-EB)/(NA-NB)))*(180/Math.PI))-(Math.atan(((EC-EB)/(NC-NB)))*(180/Math.PI)));
                        let C = ((Math.atan(((EB-EC)/(NB-NC)))*(180/Math.PI))-(Math.atan(((EA-EC)/(NA-NC)))*(180/Math.PI)));

                        let K1 = ((1)/(((Math.cos(A*(Math.PI/180)))/(Math.sin(A*(Math.PI/180))))-((Math.cos(X*(Math.PI/180)))/(Math.sin(X*(Math.PI/180))))));
                        let K2 = ((1)/(((Math.cos(B*(Math.PI/180)))/(Math.sin(B*(Math.PI/180))))-((Math.cos(Y*(Math.PI/180)))/(Math.sin(Y*(Math.PI/180))))));
                        let K3 = ((1)/(((Math.cos(C*(Math.PI/180)))/(Math.sin(C*(Math.PI/180))))-((Math.cos(Z*(Math.PI/180)))/(Math.sin(Z*(Math.PI/180))))));

                        let EP = (((K1*EA)+(K2*EB)+(K3*EC))/(K1+K2+K3));
                        let NP = (((K1*NA)+(K2*NB)+(K3*NC))/(K1+K2+K3)); 
        
                        document.getElementById('EP').innerHTML=": "+EP;
                        document.getElementById('NP').innerHTML=": "+NP;
                    } else {alert('Los angulos no cumplen las condiciones: (X+Y+Z) = 720, Z<X, Z<Y, X<Y.') }           
                }
    }
}

export default Triseccion;

//<img className='Casos' src={Caso1} alt="Casos"></img>
//                    <img className='Casos' src={Caso2} alt="Casos"></img>
//                    <img className='Casos' src={Caso3} alt="Casos"></img>