//Nivelacion Geodesica
import React from 'react';
import { Container, Button, Nav, Navbar} from 'react-bootstrap';
class NivelacionG extends React.Component {

    render() {

        let elemento =

        <Container>
        <Navbar bg="dark" variant="dark" className='nav'>
            <Navbar.Brand href="/">GEODESIA GEOMETRICA</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href=""></Nav.Link>
                <Nav.Link href="/Triseccion">Trisección</Nav.Link>
            </Nav>
        </Navbar>
        <br></br>
        <h1 className="titulo">Nivelación geodésica</h1>
        <div className="general">
            <div className="general1"> 
                <div>
                    
                </div>
                <div className="texto">
                    <h5>------------
                    </h5>
                </div>
            </div>

            <div className="general2">
                <div className="general21">
                    <label>-</label>
                    <table className="f">
                        <tr>
                            <td>
                                <label> Número de estaciones:</label>
                                <br></br>
                                <label> Número de armados:</label>
                                <br></br>
                                <label> Norte de B: </label>
                                <br></br>
                                <label> Este de B: </label>
                                <br></br>
                                <label> Angulo AP: </label>
                                <br></br>
                                <label> Angulo BP: </label>
                            </td>
                            <td>
                                <input type="number" id="NA"></input>
                                <br></br>
                                <input type="number" id="EA"></input>
                                <br></br>
                                <input type="number" id="NB"></input>
                                <br></br>
                                <input type="number" id="EB"></input>
                                <br></br>
                                <input type="number" id="alfa"></input>
                                <br></br>
                                <input type="number" id="beta"></input>
                            </td>
                        </tr><br></br><br></br>
                    </table>
                    <Button className="button" variant='outline-dark' onClick={this.cbiseccion}>Calcular</Button>
                    <br></br><br></br>
                    <label> Norte de P</label>
                    <span id="NP"></span>
                    <br></br>
                    <label> Este de P</label>
                    <span id="EP"></span>
                    <br></br>
                    <Button className="button" variant='outline-dark' onClick={this.cMapa}>Graficar</Button>
                </div>
                <div className="visor">
                    
                </div>
            </div>     
        </div>               
    </Container >;

        return elemento;
    }
}

export default NivelacionG;