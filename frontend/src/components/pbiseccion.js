//BISECCION
import React from 'react';
import { Container, Button, Nav, Navbar} from 'react-bootstrap';
import './style/pbiseccion.css';
import Mapa from './mapa';
import Imagen from './style/images/biseccion.JPG';
class Biseccion extends React.Component {

    render() {

        let elemento =

            <Container>
                <Navbar bg="dark" variant="dark" className='nav'>
                    <Navbar.Brand href="/">GEODESIA GEOMETRICA</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href=""></Nav.Link>
                        <Nav.Link href="/Triseccion">Trisección</Nav.Link>
                    </Nav>
                </Navbar>
                <br></br>
                <h1 className="titulo">Bisección</h1>
                <div className="general">
                    <div className="general1"> 
                        <div>
                            <img className='imagen1' src={Imagen} alt="imagen1"></img>
                        </div>
                        <div className="texto">
                            <h5>El metodo de bisección consiste en determinar las coordenadas de un punto P (NP, EP) conocidos dos vértices: A (NA, EA) y
                                B (NB, EB), y los angulos comprendidos entre AP y BP. 
                            </h5>
                        </div>
                    </div>

                    <div className="general2">
                        <div className="general21">
                            <label>Coordenadas planas cartesianas locales (Origen Bogotá)</label>
                            <table className="f">
                                <tr>
                                    <td>
                                        <label> Norte de A: </label>
                                        <br></br>
                                        <label> Este de A: </label>
                                        <br></br>
                                        <label> Norte de B: </label>
                                        <br></br>
                                        <label> Este de B: </label>
                                        <br></br>
                                        <label> Angulo AP: </label>
                                        <br></br>
                                        <label> Angulo BP: </label>
                                    </td>
                                    <td>
                                        <input type="number" id="NA"></input>
                                        <br></br>
                                        <input type="number" id="EA"></input>
                                        <br></br>
                                        <input type="number" id="NB"></input>
                                        <br></br>
                                        <input type="number" id="EB"></input>
                                        <br></br>
                                        <input type="number" id="alfa"></input>
                                        <br></br>
                                        <input type="number" id="beta"></input>
                                    </td>
                                </tr><br></br><br></br>
                            </table>
                            <Button className="button" variant='outline-dark' onClick={this.cbiseccion}>Calcular</Button>
                            <br></br><br></br>
                            <label> Norte de P</label>
                            <span id="NP"></span>
                            <br></br>
                            <label> Este de P</label>
                            <span id="EP"></span>
                            <br></br>
                            <Button className="button" variant='outline-dark' onClick={this.cMapa}>Graficar</Button>
                        </div>
                        <div className="visor">
                            <Mapa/>
                        </div>
                    </div>     
                </div>               
            </Container >;

        return elemento;
    };

    cbiseccion(){

        var NA = document.getElementById("NA").value;
        var NB = document.getElementById("NB").value;
        var EA = document.getElementById("EA").value;
        var EB = document.getElementById("EB").value;
        var alfa = document.getElementById("alfa").value ;
        var beta = document.getElementById("beta").value ;

        var a = ((1)/(Math.tan(alfa* (Math.PI/180))));
        var b = ((1)/(Math.tan(beta* (Math.PI/180))));

        var EP = ((NB-NA)+(EA*(b))+(EB*(a)))/((a)+(b));
        var NP = ((EA-EB)+(NA*(b))+(NB*(a)))/((a)+(b));
        
        document.getElementById('EP').innerHTML=": "+EP;
        document.getElementById('NP').innerHTML=": "+NP;          
    };

    cMapa(){


    }
}   

export default Biseccion;

//CONVERSOR DE COORDENADAS PLANAS CARTESIANAS LOCALES A GEOGRAFICAS: 

var NA = document.getElementById("NA");
var NB = document.getElementById("NB");
var EA = document.getElementById("EA");
var EB = document.getElementById("EB");
var alfa = document.getElementById("alfa") ;
var beta = document.getElementById("beta") ;

var a = ((1)/(Math.tan(alfa* (Math.PI/180))));
var b = ((1)/(Math.tan(beta* (Math.PI/180))))
var EP = ((NB-NA)+(EA*(b))+(EB*(a)))/((a)+(b));
var NP = ((EA-EB)+(NA*(b))+(NB*(a)))/((a)+(b));


var No = 109320.965;  //Norte Origen Cartesiano Bogota
var Eo = 92334.879;    // Este Origen Cartesiano Bogota
var LatO = 4.680486111;    //Latitud Geografico Origen
var LonO = -74.146591667;  //Longitud Geografico Origen
var A = 6378137.000;      //Semieje mayor del elipsoide de referencia (WGS84)
var e = 0.00669438002290;
var Pp = 2550;

var N = A/Math.sqrt((1-(e*((Math.sin(LatO*(Math.PI/180)))^2))));
var M = (A*(1-e))/Math.sqrt((1-(e*((Math.sin(LatO*(Math.PI/180)))^2)))^3);

// PARA EL PUNTO A
var dN1 = NA-No;
var dE1 = EA-Eo;

var dLat1 = ((dN1)/((1+(Pp/(A*(1-e))))*M))-((Math.tan(LatO*(Math.PI/180))/(2*M*N))*(dE1/(1+(Pp/A))));
var dLon1 = (dE1)/(N*Math.cos(1+(Pp/A)*(Math.PI/180)));

var Lat1 = LatO + dLat1;
var Lon1 = LonO + dLon1;


// PARA EL PUNTO B
var dN2 = NB-No;
var dE2 = EB-Eo;

var dLat2 = ((dN2)/((1+(Pp/(A*(1-e))))*M))-((Math.tan(LatO*(Math.PI/180))/(2*M*N))*(dE2/(1+(Pp/A))));
var dLon2 = (dE2)/(N*Math.cos(1+(Pp/A)*(Math.PI/180)));

var Lat2 = LatO + dLat2;
var Lon2 = LonO + dLon2;


// PARA EL PUNTO P
var dN3 = NP-No;
var dE3 = EP-Eo;

var dLat3 = ((dN3)/((1+(Pp/(A*(1-e))))*M))-((Math.tan(LatO*(Math.PI/180))/(2*M*N))*(dE3/(1+(Pp/A))));
var dLon3 = (dE3)/(N*Math.cos(1+(Pp/A)*(Math.PI/180)));

var Lat3 = LatO + dLat3;
var Lon3 = LonO + dLon3; 

export {Lat1, Lat2, Lat3, Lon1, Lon2, Lon3};