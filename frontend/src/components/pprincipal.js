//PAGINA PRINCIPAL
import React from 'react';
import { Container, Button } from 'react-bootstrap';
import './style/pprincipal.css';
class Principal extends React.Component {

    render() {

        let elemento =

            <Container className="general">
                <br />
                <h1 className="titulo">GEODESIA GEOMETRICA</h1><br /><br />
                <h3>Programas Segundo Parcial</h3><br></br><br></br>
                
                <div className='p2'>
                    <Button className="button" variant='outline-dark' href='/Biseccion'>Bisección</Button>
                    <Button className="button" variant='outline-dark' href='/Triseccion'>Trisección</Button>
                    <Button className="button" variant='outline-dark' href='/NivelacionGeodesica'>Nivelación Geodésica</Button>
                    <Button className="button" variant='outline-dark' href='/NivelacionTrigonometrica'>Nivelación Trigonométrica Geodésica </Button>
                    <Button className="button" variant='outline-dark' href='/DirectoInverso'>Programa directo e inverso de Latitud Media de Gauss</Button>
                </div><br />
                <br></br>
            </Container >;

        return elemento;
    }
}

export default Principal;